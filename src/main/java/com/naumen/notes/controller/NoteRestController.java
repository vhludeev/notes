package com.naumen.notes.controller;

import com.naumen.notes.model.Note;
import com.naumen.notes.service.NoteService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@RestController
public class NoteRestController {

	private final NoteService noteService;

	@Autowired
	public NoteRestController(NoteService noteService) {
		this.noteService = noteService;
	}

	@GetMapping(value = "/list")
	public List<Note> list() {
		return noteService.findAll();
	}

	@GetMapping(value = "/search")
	public List<Note> search(@RequestParam(required = false) String query) {
		if(Strings.isBlank(query)) {
			return noteService.findAll();
		}
		return noteService.search(query);
	}

	@PostMapping(value = "/save")
	public List<Note> save(@Valid @RequestBody Note note) {
		return Collections.singletonList(noteService.save(note));
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable long id) {
		noteService.delete(id);
	}
}