package com.naumen.notes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@Controller
public class NotesController {

	@GetMapping(value = "/")
	public String getMainPage() {
		return "notes";
	}
}
