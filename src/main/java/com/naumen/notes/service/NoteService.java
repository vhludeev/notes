package com.naumen.notes.service;

import com.naumen.notes.model.Note;

import java.util.List;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
public interface NoteService {

	Note save(Note note);

	List<Note> search(String text);

	List<Note> findAll();

	void delete(long id);
}
