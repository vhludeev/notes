package com.naumen.notes.service;

import com.naumen.notes.model.Note;
import com.naumen.notes.repository.NoteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@Service
@Slf4j
public class NoteServiceImpl implements NoteService {

	private final NoteRepository noteRepository;

	@Autowired
	public NoteServiceImpl(NoteRepository noteRepository) {
		this.noteRepository = noteRepository;
	}

	@Override
	public Note save(Note note) {
		return noteRepository.save(note);
	}

	@Override
	public List<Note> search(String query) {
		String resultQuery = FullTextSearchUtils.getFullTextSearchQuery(query);
		return noteRepository.fullTextSearch(resultQuery, 50, 0);
	}

	@Override
	public List<Note> findAll() {
		Page<Note> notes = noteRepository.findAllByOrderByIdDesc(PageRequest.of(0, 50));
		return notes.getContent();
	}

	@Override
	public void delete(long id) {
		try {
			noteRepository.deleteById(id);
		}
		catch (EmptyResultDataAccessException e) {
			log.info("Note with id {} already deleted", id);
		}
	}
}