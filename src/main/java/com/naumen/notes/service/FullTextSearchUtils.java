package com.naumen.notes.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vitaliy Khludeev on 10.09.18.
 */
public class FullTextSearchUtils {

	static String getFullTextSearchQuery(String query) {
		query = query.replaceAll("[^(a-zA-Z0-9А-Яа-я | )]", "");
		String normalizedQuery = query.replace("  ", " ").trim();
		List<String> words = Arrays.stream(normalizedQuery.split(" ")).filter(w -> !w.isEmpty()).collect(Collectors.toList());
		List<String> queryWords = words.stream().map(w -> w + ":*").collect(Collectors.toList());
		return String.join(" & ", queryWords).toLowerCase();
	}
}
