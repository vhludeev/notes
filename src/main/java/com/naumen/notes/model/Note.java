package com.naumen.notes.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	@NotBlank(message = "Text cannot be empty")
	private String text;
}
