package com.naumen.notes.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@RestControllerAdvice
@Slf4j
public class DefaultControllerAdvice {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public List<ValidationFailure> validationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		return fieldErrors.stream().map(f -> new ValidationFailure(f.getField(), f.getDefaultMessage())).collect(Collectors.toList());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public List<ValidationFailure> validationError(Exception e) {
		log.error(String.format("Error occurred %s", e.getLocalizedMessage()), e);
		return Collections.singletonList(new ValidationFailure(null, "Internal server error occurred"));
	}
}
