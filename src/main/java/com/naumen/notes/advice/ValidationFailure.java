package com.naumen.notes.advice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ValidationFailure {
	private String fieldName;
	private String message;
}
