package com.naumen.notes.repository;

import com.naumen.notes.model.Note;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
public interface NoteRepository extends JpaRepository<Note, Long> {

	@Query(value = "SELECT n.* " +
			"FROM note AS n " +
			"CROSS JOIN to_tsquery('russian', :query) AS q " +
			"WHERE vector @@ q " +
			"ORDER BY ts_rank(n.vector, q) DESC " +
			"LIMIT :limit OFFSET :offset ;", nativeQuery = true)
	List<Note> fullTextSearch(@Param("query") String query, @Param("limit") int limit, @Param("offset") int offset);

	Page<Note> findAllByOrderByIdDesc(Pageable page);
}
