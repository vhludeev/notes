CREATE FUNCTION update_notes_vector() RETURNS TRIGGER AS $$
BEGIN
	new.vector :=
	setweight(to_tsvector('russian', LOWER(new.title)),'A') || setweight(to_tsvector('russian', LOWER(new.text)), 'B');
	RETURN new;
END
$$ LANGUAGE plpgsql;