package com.naumen.notes.service;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Vitaliy Khludeev on 10.09.18.
 */
public class FullTextSearchUtilsTest {

	@Test
	public void fulltext_query_is_correct() {
		String fullTextSearchQuery = FullTextSearchUtils.getFullTextSearchQuery("Я работаю в компании NAUMEN");
		assertEquals(fullTextSearchQuery, "я:* & работаю:* & в:* & компании:* & naumen:*");
	}

	@Test
	public void fulltext_query_escape_incorrect_characters() {
		String fullTextSearchQuery = FullTextSearchUtils.getFullTextSearchQuery("Я *&работ^аю в компании NAUMEN");
		assertEquals(fullTextSearchQuery, "я:* & работаю:* & в:* & компании:* & naumen:*");
	}
}
