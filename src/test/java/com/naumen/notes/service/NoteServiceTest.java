package com.naumen.notes.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.naumen.notes.model.Note;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Rollback
@Transactional
public class NoteServiceTest {

	@Autowired
	private NoteService noteService;

	@Before
	public void init() throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("notes.json").getFile());
		String result = IOUtils.toString(new FileInputStream(file), StandardCharsets.UTF_8);
		ObjectMapper objectMapper = new ObjectMapper();
		List<Note> notes = objectMapper.readValue(result, new TypeReference<List<Note>>(){});
		notes.forEach(n -> noteService.save(n));
	}

	@Test
	public void saving_is_correct() {
		List<Note> notes = noteService.findAll();
		assertEquals(notes.size(), 7);
		assertEquals(notes.get(0).getTitle(), "Climate change: Protests held ahead of California summit");
	}

	@Test
	public void found_russian_text_1() throws IOException {
		List<Note> notes = noteService.search("годовщина сентябрь");
		assertEquals(notes.size(), 1);
	}

	@Test
	public void found_russian_text_2() throws IOException {
		List<Note> notes = noteService.search("безопас обеспечение");
		assertEquals(notes.size(), 1);
	}

	@Test
	public void found_english_text() throws IOException {
		List<Note> notes = noteService.search("Demands protested held");
		assertEquals(notes.size(), 1);
	}

	@Test
	public void check_rank_is_correct() throws IOException {
		List<Note> notes = noteService.search("подпорная стена понедельника");
		assertEquals(notes.size(), 3);
		assertEquals(notes.get(0).getTitle(), "В районе Улисса подпорная стена перестала содержать граффити");
		assertEquals(notes.get(1).getTitle(), "Test rank 2");
		assertEquals(notes.get(2).getTitle(), "Test rank 1");
	}

	@Test
	public void not_found_case() throws IOException {
		List<Note> notes = noteService.search("Some text example");
		assertTrue(notes.isEmpty());
	}
}
