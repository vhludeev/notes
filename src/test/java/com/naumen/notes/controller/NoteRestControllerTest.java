package com.naumen.notes.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.naumen.notes.advice.ValidationFailure;
import com.naumen.notes.model.Note;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.junit.Assert.*;

/**
 * @author Vitaliy Khludeev on 09.09.18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Rollback
@Transactional
@AutoConfigureMockMvc
public class NoteRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper mapper;

	@Test
	public void saving_is_correct() throws Exception {
		Note note = new Note().setText("Text").setTitle("Title");
		String s = mapper.writeValueAsString(note);
		mvc.perform(post("/save").contentType(MediaType.APPLICATION_JSON_UTF8).content(s)).andExpect(status().isOk());
	}

	@Test
	public void saving_with_empty_text() throws Exception {
		Note note = new Note().setTitle("Title");
		String s = mapper.writeValueAsString(note);
		MvcResult result = mvc.perform(
				post("/save").contentType(MediaType.APPLICATION_JSON_UTF8).content(s))
				.andExpect(status().isBadRequest()).andReturn();
		String content = result.getResponse().getContentAsString();
		List<ValidationFailure> failures = mapper.readValue(content, new TypeReference<List<ValidationFailure>>(){});
		assertEquals(failures.size(), 1);
		ValidationFailure validationFailure = failures.get(0);
		assertEquals(validationFailure.getFieldName(), "text");
		assertEquals(validationFailure.getMessage(), "Text cannot be empty");
	}
}
